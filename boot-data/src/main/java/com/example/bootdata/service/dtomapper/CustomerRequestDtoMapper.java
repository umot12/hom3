package com.example.bootdata.service.dtomapper;


import com.example.bootdata.domain.dto.CustomerRequestDto;

import com.example.bootdata.domain.hr.Customer;
import org.springframework.stereotype.Service;


@Service
public class CustomerRequestDtoMapper extends DtoMapperFacade<Customer, CustomerRequestDto>{
    public CustomerRequestDtoMapper (){super(Customer.class , CustomerRequestDto.class); }
}