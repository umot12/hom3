package com.example.bootdata.dao;



import com.example.bootdata.domain.hr.Employer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface EmployerJpaRepository extends JpaRepository<Employer, Long>, JpaSpecificationExecutor<Employer> {



}
