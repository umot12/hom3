DROP TABLE IF EXISTS students;

DROP TABLE IF EXISTS public.customers  CASCADE ;
CREATE TABLE public.customers (
                                 entity_id INT AUTO_INCREMENT PRIMARY KEY,
                                 name VARCHAR(250) NOT NULL,
                                 email  VARCHAR (250) NOT NULL,

                                 password VARCHAR (250) NOT NULL ,
                                 tel_num VARCHAR (250) NOT NULL,
                                 age INT NOT NULL,
                                 creation_date TIMESTAMP NOT NULL ,
                                 last_modified_date TIMESTAMP NOT NULL
);

DROP TABLE IF EXISTS public.accounts CASCADE ;
CREATE TABLE public.accounts (
                               entity_id INT AUTO_INCREMENT PRIMARY KEY,
                               number VARCHAR (250) NOT NULL,
                               currency VARCHAR(250) NOT NULL ,
                               balance INT NOT NULL ,
                               customer_id  INTEGER REFERENCES customers (entity_id),
                               creation_date TIMESTAMP NOT NULL ,
                               last_modified_date TIMESTAMP NOT NULL

);
DROP TABLE IF EXISTS public.employee CASCADE ;
CREATE TABLE public.employee(
                        entity_id INT AUTO_INCREMENT PRIMARY KEY ,
                         employer_name VARCHAR (250) NOT NULL,
                         location VARCHAR(250) NOT NULL,
                            creation_date TIMESTAMP NOT NULL ,
                        last_modified_date TIMESTAMP NOT NULL

);
DROP TABLE IF EXISTS public.employee_customer CASCADE ;
CREATE TABLE public.employee_customer
(
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    customer_id INT NOT NULL,
    employee_id INT NOT NULL ,
foreign key (customer_id) references customers(entity_id),
foreign key (employee_id) references employee (entity_id)



);
